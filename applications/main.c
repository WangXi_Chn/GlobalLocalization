/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * STM32F405 GPS MAIN SOURCE FILE
 * Used in RT-Thread Operate System
 * Used in Albatross Project
 * Yueque Doc: https://www.yuque.com/wangxi_chn/qaxke0/ggow16
 * Gitee: https://gitee.com/WangXi_Chn/GlobalLocalization
 * 
 * Change Logs:
 * Date           Author       Notes			Mail
 * 2020-09-04     WangXi   	   first version	WangXi_Chn@foxmail.com
 * 2020-09-18     WangXi   	   second version	WangXi_Chn@foxmail.com
 */
 
#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>

#include "App_GlobalPos.h"
#include "Module_AS5048.h"

APP_GLOBALPOS app_GloabalPos;

int main(void)
{
	APP_GlobalPos_Config(&app_GloabalPos);
	app_GloabalPos.Method_Run(&app_GloabalPos);
	
    return RT_EOK;
}

/* Finsh Code Begin*/
void MPU9250(int argc,char **argv)
{
	/* Get chip temperature command */
	if (!rt_strcmp(argv[1], "-t")){
		app_GloabalPos.dev_Mpu9250.GetChipTemperature(&(app_GloabalPos.dev_Mpu9250));
        rt_kprintf("\r\nInfo: Chip temperature is %d\r\n",app_GloabalPos.dev_Mpu9250.ChipTemperature);
        return;
    }
	
	if (!rt_strcmp(argv[1], "-c")){
		app_GloabalPos.dev_Mpu9250.SWICH_DEBUG = 0;
        rt_kprintf("\r\nInfo: Close the MPU9250 Debug show\r\n");
        return;
    }
	
	if (!rt_strcmp(argv[1], "-o")){
		app_GloabalPos.dev_Mpu9250.SWICH_DEBUG = 1;
        rt_kprintf("\r\nInfo: Open the MPU9250 Debug show\r\n");
        return;
    }
	
	rt_kprintf("\r\nError: Commond not found\r\n");
	
}
MSH_CMD_EXPORT(MPU9250 , MPU9250(i2c1) <-t|-c|-o>); 

/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
