/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * GLOBAL POSITION APP SOUCE FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-07     WangXi       first version        WangXi_chn@foxmail.com
 */
#include "App_GlobalPos.h"
#include "Math_Cul.h"
#include <stdbool.h>

 
static void APP_GlobalPosInit(APP_GLOBALPOS *Application);
static void APP_GlobalPosRun(APP_GLOBALPOS *Application);

static void GlobalPosLed_entry(void *parameter);
static void GlobalPosUpdate_entry(void *parameter);
static void GlobalPosDeal_entry(void *parameter);
static void GlobalPosOffset_entry(void *parameter);
static void GlobalPosFuse_entry(void *parameter);

static rt_sem_t MPUoffset_sem;

void APP_GlobalPos_Config(APP_GLOBALPOS *Application)
{
	if(	Application->Method_Init	==	NULL &&
		Application->Method_Run		==	NULL

    ){  
        /* Link the Method */
        Application->Method_Init = APP_GlobalPosInit;
		Application->Method_Run	 = APP_GlobalPosRun;
		
    }
    else{
        rt_kprintf("Warning: Module Led is Configed twice\n");
        return;
    }

    /* Device Init */
    Application->Method_Init(Application);
	
	/* Module Config */
	Module_Led_Config(&(Application->dev_Led));
	Module_File_Config(&(Application->dev_SpiFile));
	Module_UartCom_Config(&(Application->dev_UartBsp));
	Module_MPU9250_Config(&(Application->dev_Mpu9250));
	Module_AS5048_Config(&(Application->dev_As5048_left));
	Module_AS5048_Config(&(Application->dev_As5048_right));
	
    return;
}

static void APP_GlobalPosInit(APP_GLOBALPOS *Application)
{
	/* Module param list ------------------------------------------------------------------------- */
	/* LED device */
	/* Pin:	PA10 Low power enable */
	Application->dev_Led.Property_pin = GET_PIN(A, 10);
	Application->dev_Led.Property_Mode = FLASH_LED_MODE;
	Application->dev_Led.LED_TIME_CYCLE = 1500;
	Application->dev_Led.LED_TIME_OUTPUT = 150;

	/* JC24B 2.4G communication device */
	/* Pin:	PC0 Set Pin (Here not used)*/
	Application->dev_UartBsp.Property_UartDevName   = "uart3";
	Application->dev_UartBsp.Property_BaudRate		= 9600;
	Application->dev_UartBsp.Property_MyID			= 0x01;
	Application->dev_UartBsp.Property_NetID			= 0x10;
	Application->dev_UartBsp.Property_Device		= JC24B;
	Application->dev_UartBsp.Property_JC24BsetPin	= GET_PIN(C, 0);//(Here not used)
	
	/* Flash W25Q256 Spi device */
	/* Pin:	PC10 SPI3_SCK */
	/* Pin:	PC11 SPI3_MISO */
	/* Pin:	PC12 SPI3_MOSI */
	Application->dev_SpiFile.SpiBusName = "spi3";			
	Application->dev_SpiFile.SpiDevName = "spi30";
	Application->dev_SpiFile.BlockDevName = "W25Q256";		
	Application->dev_SpiFile.ChipIDRegist = 0x9F;
	Application->dev_SpiFile.BlockChipID = 0XEF4017;		
	Application->dev_SpiFile.CsPort = GPIOC;
	Application->dev_SpiFile.CsPinNum = GPIO_PIN_8;			
	Application->dev_SpiFile.IfFormatting = false;
	
	/* MPU9250 I2C device */
	/* Pin:	PB5 I2C_SCL */
	/* Pin:	PB6 I2C_SDA */
	Application->dev_Mpu9250.I2CbusName = "i2c1";
	Application->dev_Mpu9250.OffsetCulCount = 2000;
	Application->dev_Mpu9250.GPS_ANGLEMODE = RANGE360MODE;
	Application->dev_Mpu9250.GyroZaxisFilter.Property_Last_P = 0.02;
	Application->dev_Mpu9250.GyroZaxisFilter.Property_Q	= 0.001;
	Application->dev_Mpu9250.GyroZaxisFilter.Property_R = 0.543;
	
	/* As5048 device */
	Application->dev_As5048_left.SpiBusName = "spi1";
	Application->dev_As5048_left.SpiDevName = "spi10";
	Application->dev_As5048_left.CsPort		= GPIOA;
	Application->dev_As5048_left.CsPinNum	= GPIO_PIN_11;
	Application->dev_As5048_left.TurnRadius	= 1;
	
	Application->dev_As5048_right.SpiBusName = "spi1";
	Application->dev_As5048_right.SpiDevName = "spi11";
	Application->dev_As5048_right.CsPort	 = GPIOA;
	Application->dev_As5048_right.CsPinNum	 = GPIO_PIN_12;
	Application->dev_As5048_right.TurnRadius = 1;

	
	/* Data Sheet Default Value ------------------------------------------------------------------ */
}

static void APP_GlobalPosRun(APP_GLOBALPOS *Application)
{
	/* Init the sem */
	MPUoffset_sem = rt_sem_create("MPUoffset_sem", RT_NULL, RT_IPC_FLAG_FIFO);
	RT_ASSERT(MPUoffset_sem);
	
	/* Let the LED flash module show the machine ID */	
	Application->dev_Led.Method_Set(&Application->dev_Led,Application->dev_UartBsp.Property_MyID);
	
	rt_thread_t GlobalPosLed_thread = rt_thread_create("GPSLed", GlobalPosLed_entry, 
									Application,512, RT_THREAD_PRIORITY_MAX - 2, 20);
    if (GlobalPosLed_thread != RT_NULL){
        rt_thread_startup(GlobalPosLed_thread);
    }
	
	rt_thread_t GlobalPosUpdate_thread = rt_thread_create("GPSUpdate", GlobalPosUpdate_entry, 
									Application,1024, RT_THREAD_PRIORITY_MAX - 3, 20);
    if (GlobalPosUpdate_thread != RT_NULL){
        rt_thread_startup(GlobalPosUpdate_thread);
    }	
	
    rt_thread_t GlobalPosOffset_thread = rt_thread_create("GPSoffset", GlobalPosOffset_entry, 
									Application,1024, RT_THREAD_PRIORITY_MAX - 4, 20);
    if (GlobalPosOffset_thread != RT_NULL){
        rt_thread_startup(GlobalPosOffset_thread);
    }
	
	rt_thread_t GlobalPosDeal_thread = rt_thread_create("GPSDeal", GlobalPosDeal_entry, 
									Application,1024, RT_THREAD_PRIORITY_MAX - 4, 20);
    if (GlobalPosDeal_thread != RT_NULL){
        rt_thread_startup(GlobalPosDeal_thread);
    }
	
	rt_thread_t GlobalPosFuse_thread = rt_thread_create("GPSFuse", GlobalPosFuse_entry, 
									Application,1024, RT_THREAD_PRIORITY_MAX - 5, 20);
    if (GlobalPosFuse_thread != RT_NULL){
        rt_thread_startup(GlobalPosFuse_thread);
    }
}
 
/* thread entry list ----------------------------------------------------------------------------- */
static void GlobalPosLed_entry(void *parameter)
{
	APP_GLOBALPOS *Application = (APP_GLOBALPOS *)parameter;
	while(1)
	{
		rt_thread_mdelay(1);
		Application->dev_Led.Method_Handle(&(Application->dev_Led));
	}	
}

static void GlobalPosUpdate_entry(void *parameter)
{
	APP_GLOBALPOS *Application = (APP_GLOBALPOS *)parameter;
    while (1)
	{
		rt_thread_mdelay(100);
		Application->dev_UartBsp.Value_AimID = 0x10;
		Application->dev_UartBsp.Method_Send(&(Application->dev_UartBsp),gytempreture_ID,Application->dev_Mpu9250.ChipTemperature);
		Application->dev_UartBsp.Method_Send(&(Application->dev_UartBsp),gylingdata_ID,Application->dev_Mpu9250.OffsetGyroscope[Z_AXIS]);
		Application->dev_UartBsp.Method_Send(&(Application->dev_UartBsp),qystartdata_ID,Application->dev_Mpu9250.OringalGyroscope[Z_AXIS]);
		Application->dev_UartBsp.Method_Send(&(Application->dev_UartBsp),gylvdata_ID,Application->dev_Mpu9250.AngleGyroscope[Z_AXIS]);
	}
}

static void GlobalPosOffset_entry(void *parameter)
{
	APP_GLOBALPOS *Application = (APP_GLOBALPOS *)parameter;
	Application->dev_Mpu9250.GetOffsetGyro(&(Application->dev_Mpu9250));
	Application->dev_As5048_left.GetOffsetData(&(Application->dev_As5048_left));
	Application->dev_As5048_right.GetOffsetData(&(Application->dev_As5048_right));
	
	rt_sem_release(MPUoffset_sem);
}

static void GlobalPosDeal_entry(void *parameter)
{
	rt_uint32_t Count;
	
	APP_GLOBALPOS *Application = (APP_GLOBALPOS *)parameter;
	
    rt_sem_take(MPUoffset_sem, RT_WAITING_FOREVER);
    rt_sem_delete(MPUoffset_sem);
	
    while (1)
	{
		rt_thread_mdelay(1);
		Application->dev_Mpu9250.GetAngleGyro(&(Application->dev_Mpu9250));	
		Application->dev_As5048_left.GetDistance(&(Application->dev_As5048_left));
		Application->dev_As5048_right.GetDistance(&(Application->dev_As5048_right));
		
		Count++;
		if(Count>=100)
		{
			Count = 0;
			Application->dev_Mpu9250.GetChipTemperature(&(Application->dev_Mpu9250));
			//rt_kprintf("info:X is %d, Y is %d\r\n",(rt_int32_t)Application->dev_As5048_left.ECD_Distance,(rt_int32_t)Application->dev_As5048_right.ECD_Distance);
		}
    }
}
 
static void GlobalPosFuse_entry(void *parameter)
{
	APP_GLOBALPOS *Application = (APP_GLOBALPOS *)parameter;
	
	while (1)
	{
		rt_thread_mdelay(500);
		float tempPos_X = Application->dev_As5048_left.ECD_Distance;
		float tempPos_Y = -(Application->dev_As5048_right.ECD_Distance);
		float tempPos_Angle = Application->dev_Mpu9250.AngleGyroscope[Z_AXIS];
		
		Application->Pos_X = MATH_COS_ANG(tempPos_Angle + 45)*tempPos_X - MATH_SIN_ANG(tempPos_Angle + 45)*tempPos_Y;
		Application->Pos_Y = MATH_SIN_ANG(tempPos_Angle + 45)*tempPos_X + MATH_COS_ANG(tempPos_Angle + 45)*tempPos_Y;
		Application->Pos_Angle = tempPos_Angle;
		rt_kprintf("info:%d, %d, %d\r\n",(rt_int32_t)Application->Pos_X,(rt_int32_t)Application->Pos_Y,(rt_int32_t)Application->Pos_Angle);
	}
}
 
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/
 

