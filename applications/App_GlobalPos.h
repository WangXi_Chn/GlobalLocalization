/*
 * Copyright (c) 2020 - ~, HIT_HERO Team
 *
 * PCCOMTEST APP HEAD FILE
 * Used in RT-Thread Operate System
 *
 * Change Logs:
 * Date           Author       Notes                Mail
 * 2020-09-06     WangXi       first version        WangXi_chn@foxmail.com
 */
#ifndef _APP_GLOBALPOS_H_
#define _APP_GLOBALPOS_H_

#include <rtthread.h>
#include <rtdevice.h>
#include <board.h>
 
#include "Module_LED.h"
#include "Module_FILE.h"
#include "Module_UartCom.h"
#include "Module_MPU9250.h" 
#include "Module_AS5048.h"

struct _APP_GLOBALPOS
{
    /* Property */
	MODULE_LED 		dev_Led;
	MODULE_FILE 	dev_SpiFile;
	MODULE_UARTCOM 	dev_UartBsp;
	MODULE_MPU9250  dev_Mpu9250;
	MODULE_AS5048	dev_As5048_left;
	MODULE_AS5048	dev_As5048_right;
	
	/* Value */
	UARTCOM_PACKETID Value_PacketID;
	float	Pos_X;
	float	Pos_Y;
	float	Pos_Angle;
	
	/* Method */
    void (*Method_Init)(struct _APP_GLOBALPOS *Application);
	void (*Method_Run)(struct _APP_GLOBALPOS *Application);

};
typedef struct _APP_GLOBALPOS APP_GLOBALPOS;

void APP_GlobalPos_Config(APP_GLOBALPOS *Application);
 
//Machine_ID define--------------------------------------------------------------

//Packet_ID define---------------------------------------------------------------
//陀螺仪
//陀螺仪芯片温度 gytempreture 0X01
#define gytempreture_ID 0x01
//陀螺仪Z轴零偏数据 gylingdata 0X02
#define gylingdata_ID 0x02
//陀螺仪Z轴原始数据 qystartdata 0X03
#define qystartdata_ID 0X03
//陀螺仪Z轴滤波数据 gylvdata 0X04
#define gylvdata_ID 0X04
//陀螺仪计算角度 gyangle 0X05
#define gyangle_ID 0X05
//陀螺仪Kalman滤波器参数  估算协方差P gyP 0X06
#define gyP 0x06
//陀螺仪Kalman滤波器参数  过程噪声协方差Q gyQ 0X07
#define gyQ 0X07
//陀螺仪Kalman滤波器参数  观测噪声协方差R gyR 0X08
#define gyR 0X08

//磁力计
//磁位计X轴原始数据 ma 0X11
#define ma 0X11
//磁位计X轴滤波数据 malvX 0X12
#define malvX 0X12
//磁位计Y轴原始数据 mastartY 0X13
#define mastartY 0X13
//磁位计Y轴滤波数据 malvY 0X14
#define malvY 0X14
//磁位计计算角度 maAngle 0X15
#define maAngle 0X15
//设置零偏采样值个数 maNumber 0X16
#define maNumber 0X16
//磁位计X轴零偏数据 malingX 0X17
#define malingX 0X17
//磁位计Y轴零偏数据 malingY 0X18
#define malingY 0X18
//磁位计圆化率 macircle 0X19
#define macircle 0X19
//磁位计Kalman滤波器参数  估算协方差P maP 0X1A
#define maP 0X1A
//磁位计Kalman滤波器参数  过程噪声协方差Q maQ 0X1B
#define maQ 0X1B
//磁位计Kalman滤波器参数  观测噪声协方差R maR 0X1C
#define maR 0X1C

//整体部分读取
//互补滤波角度 allangle 0X21
#define allangle 0X21
//全局定位X坐标值 allX 0X22
#define allX 0x22
//全局定位Y坐标值 allY 0X23
#define allY 0x23
//全局定位姿态角值 allziangle 0X24
#define allziangle 0X24

//模式部分
//选择360° mo360 0X31
#define mo360 0X31
//±180°模式 mo180 0X32
#define mo180 0X32

//控制部分读取
//装置目标位置值 cogoallocation 0X41
#define cogoallocation 0X41
//装置实际位置值 coactuallocation 0X42
#define coactuallocation 0X42
//装置目标角度值 cogoalangle 0X43
#define cogoalangle 0X43
//装置实际位置值 coactualangle 0X44
#define coactualangle 0X44
//装置坐标值X coX 0X45
#define coX 0X45
//装置坐标值Y coY 0X46
#define coY 0X46
//装置姿态角 coangle 0X47
#define coangle 0X47

 
#endif
/************************ (C) COPYRIGHT 2020 WANGXI **************END OF FILE****/

